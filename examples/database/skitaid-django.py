import skitai
import sys; sys.path.insert (0, 'backend/models')

skitai.mount ("/", "backend/models/wsgi:application")
skitai.run ()