import os
import sys
import atila
import skitai

BASE_DIR = os.path.dirname (__file__)

def __config__ (pref):
    sys.path.insert (0, os.path.join (BASE_DIR, 'models'))
    skitai.mount ("/", os.path.join (BASE_DIR, 'models/wsgi:application'), pref, name = 'models')
    skitai.mount ("/static", os.path.join (BASE_DIR, 'models/static'))
    skitai.mount ("/media", os.path.join (BASE_DIR, 'models/media'))

def __setup__ (context, app):
    app.secret_key = app.config.SETTINGS.SECRET_KEY

def __app__ ():
    return atila.Atila (__name__)

def __mount__ (context, app):
    from orm.blog.models import Post

    @app.route ("/api/recent-posts")
    def index (context):
        return context.API (result = list (Post.objects.all ().values ()))
