import sys, os
from rs4 import pathtool

def __config__ (pref):
    import skitai
    import settings

    pathtool.mkdir (settings.STATIC_ROOT)
    skitai.mount (settings.STATIC_URL, settings.STATIC_ROOT)
    skitai.log_off (settings.STATIC_URL)
    pref.config.SETTINGS = settings
