from django.contrib import admin
from django.urls import include, path
from orm.blog import urls as blog_urls

urlpatterns = [
    path ('admin/', admin.site.urls),
    path ('api/', include (blog_urls))
]