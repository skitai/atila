from django.db import models

class Post (models.Model):
    title = models.CharField (max_length=200, unique=True)
    updated_on = models.DateTimeField (auto_now= True)
    content = models.TextField ()
