from rest_framework import routers
from rest_framework import viewsets
from django.urls import path, include
from .serializers import PostSerializer
from .models import Post

class PostViewSet (viewsets.ModelViewSet):
    http_method_names = ['get', 'head']
    serializer_class = PostSerializer
    queryset = Post.objects.all ()

router = routers.DefaultRouter ()
router.register('posts', PostViewSet)
urlpatterns = [
    path('', include (router.urls))
]