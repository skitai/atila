#! /usr/bin/env python3
import skitai
import backend

with skitai.preference () as pref:
    pref.config.MAX_UPLOAD_SIZE = 1 * 1024 * 1024 * 1024
    skitai.mount ('/', backend, pref)

skitai.run (ip = '0.0.0.0', port = 5000, name = 'atila-app', workers = 1)
