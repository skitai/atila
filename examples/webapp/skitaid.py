import skitai
import os
if os.path.isfile ("backend/resources/sqlite3.db"):
    os.remove ("backend/resources/sqlite3.db")
import backend

# mount app with runtime preference
with skitai.preference () as pref:
    # mount app
    pref.debug = True
    pref.use_reloader = True
    pref.config.resource_dir = skitai.joinpath ('resources')

    skitai.mount ("/", backend, pref = pref)
    skitai.mount ("/", './backend/static')

# run server
skitai.run (port = 30371)
