# 2017. 3. 13 by Hans Roh (hansroh@gmail.com)

import skitai
import os
from atila import Atila
import shutil
import delune

def __config__ (pref):
	skitai.register_g (delune.SIG_UPD)
	assert pref.config.resource_dir
	pref.config.resource_dir = os.path.abspath (pref.config.resource_dir)

def __app__ ():
	return Atila (__name__)

def __setup__ (context, app):
	from . import apis, templates, websocket
	app.mount ('/apis', apis)
	app.mount ('/templates', templates)
	app.mount ('/websocket', websocket)

	BASE_DIR = os.path.dirname (__file__)
	if not os.path.isdir (os.path.join (BASE_DIR, 'resources')):
		os.mkdir (os.path.join (BASE_DIR, 'resources'))
		shutil.copy (os.path.join (BASE_DIR, 'sqlite3.db'), os.path.join (BASE_DIR, 'resources', 'sqlite3.db'))

def __mount__ (context, app):
	@app.route ("/")
	def index (context):
		html = (
			"<html>"
			"<header><title>Atila Example</title></header>"
			"<body>"
			"<h1><img src='{}' width=50 align='absmiddle'> Atila Example</h1>"
			"<div>This page is created by simple string, For viewing below link you need to install jinja2 using:</div>"
			"<blockquote>pip install jinja2</blockquote>"
			"<ul>"
			"<li><a href='{}'>View templated page example</a></li>"
			"</ul>"
			"</body>"
			"</html>"
		).format (
			context.ab ("/avatar.png"),
			context.ab ('templates:index')
		)
		return html

	@app.route ("/status")
	@app.permission_required (["index", "replica"])
	def status (context):
		return context.status ()
