import random, time, os

def background_job (wait = 2):
    time.sleep (wait)
    return [1, 2, 3]

def __mount__ (context):
    app = context.app
    @app.route ('', methods = ["GET"])
    def index (context, message = ''):
        return context.API (
            your_message = message,
            referer = context.request.env.get ("HTTP_REFERER"),
            user_agent = context.request.env ["HTTP_USER_AGENT"]
        )

    @app.route ("/mixing")
    def mixing (context):
        def respond (context, tasks):
            a, b, c, d, e, f = tasks.fetch ()
            return context.API (a =a, b = b, c = c, d = d, e = e, f = f)
        return context.Tasks (
            context.Mask ([{"name": "A", "score": 40}, {"name": "A", "score": 40}]),
            context.Mask ("@pypi/project/rs4/"),
            context.Thread (time.sleep, args = (0.3,)),
            context.Process (time.sleep, args = (0.3,)),
            context.Mask ('mask'),
            context.Subprocess ("ls"),
        ).then (respond)

    @app.route ("/mixing_taskmap")
    def mixing_taskmap (context):
        def sleep (timeout):
            time.sleep (timeout)
            return 1
        data = context.Tasks (
            context.Thread (time.sleep, args = (0.3,)),
            context.Process (time.sleep, args = (0.3,)),
            a = context.Mask ([{"name": "A", "score": 40}, {"name": "A", "score": 40}]),
            b = context.Mask ("@pypi/project/rs4/"),
            e = context.Mask ('mask'),
            f = context.Subprocess ("ls")
        ).dict ()
        return context.API (data)

    @app.route ("/mixing_map")
    def mixing_map (context):
        def sleep (timeout):
            time.sleep (timeout)
            return 1
        return context.Map (
            context.Thread (time.sleep, args = (0.3,)),
            context.Process (time.sleep, args = (0.3,)),
            a = context.Mask ([{"name": "A", "score": 40}, {"name": "A", "score": 40}]),
            b = context.Mask ("@pypi/project/rs4/"),
            e = context.Mask ('mask'),
            f = context.Subprocess ("ls")
        )

    @app.route ("/sp_map")
    def sp_map (context):
        return context.Map ('210 Success', a = context.Subprocess ("ls"))

    @app.route ("/sp_mapped")
    def sp_mapped (context):
        return context.Mapped ("210 Success", context.Tasks (a = context.Subprocess ("ls")))

    @app.route ("/th_map")
    def th_map (context):
        def environ ():
            return dict (os.environ)
        return context.Map ('210 Success', a = context.Thread (environ))

    @app.route ("/mixing_nested_taskmap")
    def mixing_nested_taskmap (context):
        def sleep (timeout):
            time.sleep (timeout)
            return 1

        nested = context.Tasks (
            context.Process (time.sleep, args = (0.3,)),
            e = context.Mask ('mask'),
            f = context.Subprocess ("ls")
        )
        data = context.Tasks (
            context.Thread (time.sleep, args = (0.3,)),
            a = context.Mask ([{"name": "A", "score": 40}, {"name": "A", "score": 40}]),
            b = context.Mask ("@pypi/project/rs4/"),
            nested__dict = nested
        ).dict ()

        return context.API (data)

    @app.route ("/db2")
    def db2 (context):
        def respond (context, rs):
            return context.API (rows = rs.fetch ())
        return context.Mask ([{"name": "A", "score": 40}, {"name": "A", "score": 40}]).then (respond)

    @app.route ("/rest-api")
    def restapi (context):
        req = context.Mask ("@pypi/project/rs4/")
        return context.API (
            result = req.fetch ()
        )

    @app.route ("/rest-api2")
    def restapi2 (context):
        def respond (context, rs):
            return context.API (result = rs.fetch ())
        return context.Mask ("@pypi/project/rs4/").then (respond)

    @app.route ("/thread")
    def thread (context):
        s = time.time ()
        req = context.Thread (background_job, 2)
        time.sleep (2)
        return context.API (
            duration = time.time () - s,
            result = req.fetch ()
        )

    @app.route ("/thread2")
    def thread2 (context):
        def respond (context, rs):
            return context.API (result = rs.fetch ())
        return context.Thread (background_job, 2).then (respond)

    @app.route ("/process")
    def process (context):
        s = time.time ()
        req = context.Process (background_job, 2)
        time.sleep (2)
        return context.API (
            duration = time.time () - s,
            result = req.fetch ()
        )

    @app.route ("/subprocess")
    def subprocess (context):
        req = context.Subprocess ("ls -lf")
        return context.API (
            result = req.fetch ().split ()
        )

    @app.route ("/urlfor")
    def urlfor (context):
        return context.API (
            urls = [
                context.ab (index, 'urlfor'),
                context.ab (index),
                context.ab (subprocess),
                context.ab ('templates:index'),
                context.ab ('templates:index', 'urlfor'),
                context.ab ('templates:api')
            ]
        )

    @app.route ("/map")
    def map (context):
        return context.Map (
            context.Mask ([{"name": "A", "score": 40}, {"name": "A", "score": 40}]),
            context.Mask ("@pypi/project/rs4/"),
            a = 123,
            b = '456',
            c = context.Thread (time.sleep, args = (0.3,)),
            d = context.Process (time.sleep, args = (0.3,)),
            e = context.Mask ('mask'),
            f = context.Subprocess ("ls")
        )