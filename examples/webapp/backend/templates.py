
def __mount__ (context, app):
    @app.route ('', methods = ["GET"])
    def index (context, message = 'Hellow, Atila'):
        return context.render ('index.jinja', your_message = message)

    @app.route ('/api-examples', methods = ["GET"])
    def api (context, message = 'Hellow, Atila'):
        return context.render ('api.jinja', your_message = message)
