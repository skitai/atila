from atila import Atila
from services import apis
from services import templates
from services import websocket
import skitai
import os
import shutil

app = Atila (__name__)

app.mount ('/apis', apis)
app.mount ('/templates', templates)
app.mount ('/websocket', websocket)

BASE_DIR = os.path.dirname (__file__)
if not os.path.isdir (os.path.join (BASE_DIR, 'resources')):
    os.mkdir (os.path.join (BASE_DIR, 'resources'))
    shutil.copy (os.path.join (BASE_DIR, 'sqlite3.db'), os.path.join (BASE_DIR, 'resources', 'sqlite3.db'))

@app.route ("/")
def index (context):
    html = (
        "<html>"
        "<header><title>Atila Example</title></header>"
        "<body>"
        "<h1><img src='{}' width=50 align='absmiddle'> Atila Example</h1>"
        "<div>This page is created by simple string, For viewing below link you need to install jinja2 using:</div>"
        "<blockquote>pip install jinja2</blockquote>"
        "<ul>"
        "<li><a href='{}'>View templated page example</a></li>"
        "</ul>"
        "</body>"
        "</html>"
    ).format (
        context.ab ("/avatar.png"),
        context.ab ('templates:index')
    )
    return html
