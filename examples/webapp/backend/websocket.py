import skitai

def __mount__ (context):
    app = context.app
    @app.route ("/echo")
    @app.websocket (skitai.WS_SIMPLE | skitai.WS_NOPOOL)
    def echo (context, message = ''):
        return "echo: " + message
