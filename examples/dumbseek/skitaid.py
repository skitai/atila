import skitai
import os
import dumbseek

# mount app with runtime preference
with skitai.preference () as pref:
    # mount app
    pref.debug = True
    pref.use_reloader = True
    skitai.mount ("/", dumbseek, pref = pref)
# run server
skitai.run (port = 30371)
