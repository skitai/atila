from .. import analyzer
from . import db

def index (doc):
    doc_ids = list (db.DOCUMENTS.keys ())
    if not doc_ids:
        doc_id = 0
    else:
        doc_id = max (doc_ids) + 1

    db.DOCUMENTS [doc_id] = doc
    for token in analyzer.analyze (doc):
        if token not in db.INVERTED_INDEX:
            db.INVERTED_INDEX [token] = set ()
        db.INVERTED_INDEX [token].add (doc_id)
    return doc_id