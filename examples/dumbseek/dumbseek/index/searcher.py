from .. import analyzer
from . import db

def search (query):
    results = None
    for token in analyzer.analyze (query):
        if token not in db.INVERTED_INDEX:
            return []
        doc_ids = db.INVERTED_INDEX.get (token, set ())
        if results is None:
            results = doc_ids
            continue
        results = results.intersection (doc_ids)
    return [db.DOCUMENTS [doc_id] for doc_id in sorted (list (results))]
