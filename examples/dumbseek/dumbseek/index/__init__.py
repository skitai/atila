def __mount__ (context, app):
    from . import indexer
    from . import searcher
    from . import db

    @app.route ("/documents", methods = ["POST", "OPTIONS"])
    def index_document (context, document):
        doc_id = indexer.index (document)
        return context.API (
            "201 Created",
            url = context.urlfor (get_document, doc_id)
        )

    @app.route ("/documents", methods = ["GET"])
    def search_document (context, q):
        return context.API (
            result = searcher.search (q)
        )

    @app.route ("/documents/<int:doc_id>", methods = ["GET"])
    def get_document (context, doc_id):
        try:
            return context.API (
                document = db.DOCUMENTS [doc_id]
            )
        except KeyError:
            raise context.HttpError ("404 Not Found")

