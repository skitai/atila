__version__ = "1.0"

NAME = "Dumb Seek"

def __app__ ():
    from atila import Atila
    return Atila (__name__)

def __setup__ (context, app):
    from . import analyzer
    from . import index

    app.mount ("/api", analyzer)
    app.mount ("/api", index)

def __mount__ (context, app):
    @app.route ("/")
    def index (context):
        return context.API (
            app = NAME
        )