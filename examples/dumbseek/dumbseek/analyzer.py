def analyze (query):
    return query.lower ().split ()

def __mount__ (context, app):
    @app.route ("/tokens", methods = ["GET", "POST", "OPTIONS"])
    def tokens (context, query):
        return context.API (
            result = analyze (query)
        )
