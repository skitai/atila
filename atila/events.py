EVT_REQ_STARTED = "before_request"
EVT_REQ_FAILED = "request_failed"
EVT_REQ_SUCCESS = "request_success"
EVT_REQ_TEARDOWN = "teardown_request"

EVT_BEFORE_MOUNT = 'before_mount'
EVT_MOUNTED = 'mounted'
EVT_BEFORE_RELOAD = 'before_reload'
EVT_RELOADED = 'reloaded'
EVT_BEFORE_UMOUNT = 'before_umount'
EVT_UMOUNTED = 'umounted'
EVT_MOUNTED_RELOADED = 'mounted_or_reloaded'

EVT_SPEC_CREATED = 'spec:exposed' # related skitai.wastuff.api
