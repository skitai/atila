# App Middlewares

```python
import time

@app.middleware ('perf')
def add_process_time (context, app, call_next):
    start_time = time.time ()
    result = call_next ()
    context.response.set_header ("X-Process-Time", time.time () - start_time)
    return result
```

It is same as:
```python
app.add_middleware ('perf', add_process_time)
```

- Note 1: Multiple middlewares are executed by registration order.
- Note 2: Every middlewares will be called after `__request__` hooks.



## Using Async Middleware
In async middleware, be careful to call `call_next`.
- Do not mess with both ones. Add sync midllewares first, async ones later.
- In async middleware, use `result = await call_next ()`.

```python
@app.middleware ('perf')
async def add_process_time (context, app, call_next):
    ...
    # if next middleware is coroutine
    result = await call_next ()
    ...
    return result
```

