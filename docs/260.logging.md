# Logging and Traceback
```python
@app.route ("/")
def sum ():
    context.log ("called index", "info")
    try:
        ...
    except:
        context.log ("exception occured", "error")
        context.traceback ()
    context.log ("done index", "info")
```
- context.log (msg, category = "info")
- context.traceback (id = "") # id is used as fast searching log line
  for debug, if not given, id will be *Global transaction ID/Local transaction ID*