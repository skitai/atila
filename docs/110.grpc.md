# gRPC

## Unary RPC

```python
@app.route ("/GetFeature")
def GetFeature (context, point):
	feature = get_feature(db, point)
    time.sleep (0.5)
	if feature is None:
		return route_guide_pb2.Feature(name="", location=point)
	else:
		return feature
```

### Async version
*New in verson 0.24*
```python
@app.route ("/GetFeature")
async def GetFeature (context, point):
	feature = get_feature(db, point)
	await asyncio.sleep (0.5)
	if feature is None:
		return route_guide_pb2.Feature(name="", location=point)
	else:
		return feature
```

## Async Streaming RPC
*New in verson 0.24*

### Response Streaming
Use `yield` for response streaming.
```python
@app.route ("/ListFeatures", stream = True)
async def ListFeatures (context, rectangle):
	for feature in db:
		yield feature
```

### Request Streaming
Use `async for` for request streaming.
```python
@app.route ("/ListFeatures", stream = True)
async def ListFeatures (context, point_iter):
	async for point in point_iter:
        ...
	return route_guide_pb2.RouteSummary (point_count=point_count)
```

### Bidirectional Streaming
Use both `async for` and `yield`
```python
@app.route ("/RouteChat", stream = True)
async def RouteChat (context, note_iter):
	async for new_note in note_iter:
		for prev_note in prev_notes:
			yield prev_note
```












**[<< Back To README](https://gitlab.com/skitai/atila/-/blob/master/README.md)**
