"""
Hans Roh 2015 -- http://osp.skitai.com
License: BSD
"""
import re
import sys
import os
import shutil, glob
import codecs
from warnings import warn
from rs4.webkit import markdown

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

with open('atila/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*"(.*?)"',fd.read(), re.M).group(1)

if 'build' in sys.argv or 'bdist_wheel' in sys.argv:
    markdown.merge ('docs', 'README.md', 'https://gitlab.com/skitai/atila/-/blob/master/docs/', k = 3)

if 'publish' in sys.argv:
    markdown.merge ('docs', 'README.md', 'https://gitlab.com/skitai/atila/-/blob/master/docs/', k = 3)
    os.system ('{} setup.py bdist_wheel'.format (sys.executable))
    whl = glob.glob ('dist/atila-{}-*.whl'.format (version))[0]
    os.system ('twine upload --verbose {}'.format (whl))
    os.system ('pip3 install -U --pre atila==1000 > /dev/null 2>&1')
    sys.exit ()

classifiers = [
    'License :: OSI Approved :: MIT License',
    'Development Status :: 4 - Beta',
    'Topic :: Internet :: WWW/HTTP :: WSGI',
    'Environment :: Console',
    'Topic :: Internet',
    'Topic :: Software Development :: Libraries :: Python Modules',
    'Intended Audience :: Developers',
    'Programming Language :: Python :: 3.7',
    'Programming Language :: Python :: 3.8',
    'Programming Language :: Python :: 3.9',
    'Programming Language :: Python :: 3.10',
    'Programming Language :: Python :: 3.11',
    'Programming Language :: Python :: 3.12',
    'Programming Language :: Python :: Implementation :: CPython'
]

packages = [
    'atila',
    'atila.app',
    'atila.was',
    'atila.executors',
    'atila.collectors',
    'atila.patches',
    'atila.collabo',
    'atila.collabo.django',
    'atila.collabo.django.commands',
]

package_dir = {'atila': 'atila'}
package_data = {}

install_requires = [
    "skitai>=0.57.0"
]


if __name__ == "__main__":
    with codecs.open ('README.md', 'r', encoding='utf-8') as f:
        long_description = f.read()
    # long_description = "[Documentation](https://gitlab.com/skitai/atila/-/blob/master/README.md)"

    setup (
        name='atila',
        version=version,
        description='Atila Framework',
        long_description=long_description,
        long_description_content_type = 'text/markdown',
        url = 'https://gitlab.com/sitai/atila',
        author='Hans Roh',
        author_email='hansroh@gmail.com',
        packages=packages,
        package_dir=package_dir,
        package_data = package_data,
        entry_points = {},
        license='MIT',
        platforms = ["posix", "nt"],
        download_url = "https://pypi.python.org/pypi/atila",
        install_requires = install_requires,
        classifiers=classifiers
    )
